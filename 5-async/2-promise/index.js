function fetchData(url) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    // <-- start
    // TODO 22: 通过Promise实现异步请求
    xhr.onerror = () => {
      reject(new Error(xhr.statusText));
    };
    xhr.onload = () => {
      resolve(xhr.responseText);
    };

    xhr.open('get', url, true);
    xhr.send(null);
    // end -->
  });
}

const URL = 'http://localhost:3000/api';
fetchData(URL)
  .then(result => {
    document.writeln(JSON.parse(result).name);
  })
  .catch(error => {
    console.error(error);
  });

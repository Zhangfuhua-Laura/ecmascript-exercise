function chooseMultiplesOfThree(collection) {
  // TODO 1: 在这里写实现代码
  return collection.filter(item => item % 3 === 0);
}

function chooseNoRepeatNumber(collection) {
  // TODO 2: 在这里写实现代码
  const result = [];
  collection.forEach(function(item) {
    if (!result.includes(item)) {
      result.push(item);
    }
  });
  return result;
}

export { chooseMultiplesOfThree, chooseNoRepeatNumber };
